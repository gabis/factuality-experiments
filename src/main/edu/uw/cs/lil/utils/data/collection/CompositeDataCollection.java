package edu.uw.cs.lil.utils.data.collection;

import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class CompositeDataCollection<DI> implements IDataCollection<DI> {
	private final List<IDataCollection<DI>>	collections;
	private Predicate<IDataCollection<DI>>	componentFilter;

	public CompositeDataCollection(List<IDataCollection<DI>> collections) {
		this(collections, x -> true);
	}

	public CompositeDataCollection(List<IDataCollection<DI>> collections,
			Predicate<IDataCollection<DI>> componentFilter) {
		this.collections = collections;
		this.componentFilter = componentFilter;
	}

	@Override
	public Iterator<DI> iterator() {
		return stream().iterator();
	}

	@Override
	public int size() {
		return collections.stream().filter(componentFilter)
				.mapToInt(IDataCollection::size).sum();
	}

	@Override
	public Stream<DI> stream() {
		return collections.stream().filter(componentFilter)
				.flatMap(IDataCollection::stream);
	}
}