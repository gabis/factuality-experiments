package edu.uw.cs.lil.utils.data.text;

import java.io.Serializable;

import edu.uw.cs.lil.utils.data.collection.IDataCollection;

public interface IDocument<T extends ISituatedToken<? extends ISentence<T>>, S extends ISituatedSentence<T, ? extends IDocument<T, S>>>
		extends Serializable, IDataCollection<S> {

	S get(int i);

	String getId();
}