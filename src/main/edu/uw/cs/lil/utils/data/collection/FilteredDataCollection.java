package edu.uw.cs.lil.utils.data.collection;

import java.util.Iterator;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class FilteredDataCollection<DI> implements IDataCollection<DI> {
	private final IDataCollection<DI>	data;
	private final Predicate<DI>			filter;
	private final int					size;

	public FilteredDataCollection(IDataCollection<DI> data, Predicate<DI> filter) {
		this.data = data;
		this.filter = filter;
		this.size = Long.valueOf(stream().count()).intValue();
	}

	@Override
	public Iterator<DI> iterator() {
		return stream().iterator();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Stream<DI> stream() {
		return data.stream().filter(filter);
	}
}