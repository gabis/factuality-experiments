package edu.uw.cs.lil.utils.data.collection;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShuffledDataCollection<A> implements IDataCollection<A> {
	protected final List<A>	shuffledData;

	public ShuffledDataCollection(IDataCollection<A> data) {
		this.shuffledData = data.stream().collect(Collectors.toList());
		Collections.shuffle(this.shuffledData, new Random(0));
	}

	@Override
	public Iterator<A> iterator() {
		return shuffledData.iterator();
	}

	@Override
	public int size() {
		return shuffledData.size();
	}

	@Override
	public Stream<A> stream() {
		return shuffledData.stream();
	}
}