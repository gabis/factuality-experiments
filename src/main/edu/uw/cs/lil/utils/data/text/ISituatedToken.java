package edu.uw.cs.lil.utils.data.text;


public interface ISituatedToken<STATE> extends IToken {
	STATE getState();
}
