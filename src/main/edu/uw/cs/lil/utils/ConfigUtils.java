package edu.uw.cs.lil.utils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.configuration.Configuration;

public class ConfigUtils {
	private ConfigUtils() {
	}

	public static File getFile(String key, Configuration config) {
		return new File(config.getString(key));
	}

	public static List<File> getFiles(String key, Configuration config) {
		return getStringList(key, config).stream().map(File::new)
				.collect(Collectors.toList());
	}

	public static Optional<File> getOptionalFile(String key,
			Configuration config) {
		return config.containsKey(key) ? Optional.of(getFile(key, config))
				: Optional.empty();
	}

	public static Optional<String> getOptionalString(String key,
			Configuration config) {
		return config.containsKey(key) ? Optional.of(config.getString(key))
				: Optional.empty();
	}

	public static List<String> getStringList(String key, Configuration config) {
		return Arrays.asList(config.getStringArray(key));
	}

	public static Set<String> getStringSet(String key, Configuration config) {
		return Arrays.stream(config.getStringArray(key)).collect(
				Collectors.toSet());
	}
}
