package edu.uw.cs.lil.utils.vector;

import java.io.Serializable;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import edu.uw.cs.lil.utils.data.tuple.Pair;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;

public interface ISparseVector extends Serializable {
	public static final long	serialVersionUID	= 2818003820022144955L;

	ISparseVector add(ISparseVector other);

	ISparseVector addTimes(double coefficient, ISparseVector other);

	ISparseVector crossProduct(ISparseVector other);

	double dotProduct(ISparseVector other);

	void forEach(BiConsumer<ISparseKey, Double> f);

	double get(ISparseKey key);

	Set<ISparseKey> getKeys();

	Stream<ISparseKey> keyStream();

	default double l1Norm() {
		return stream().mapToDouble(entry -> entry.second()).sum();
	}

	default double l2Norm() {
		return stream().mapToDouble(entry -> entry.second() * entry.second())
				.sum();
	}

	ISparseVector mapEntries(
			BiFunction<ISparseKey, Double, ISparseKey> keyFunction,
			BiFunction<ISparseKey, Double, Double> valueFunction);

	default ISparseVector mapKeys(
			BiFunction<ISparseKey, Double, ISparseKey> keyFunction) {
		return mapEntries(keyFunction, (k, v) -> v);
	}

	default ISparseVector mapValues(
			BiFunction<ISparseKey, Double, Double> valueFunction) {
		return mapEntries((k, v) -> k, valueFunction);
	}

	int size();

	Stream<Pair<ISparseKey, Double>> stream();

	ISparseVector times(double coefficient);

}
