package edu.uw.cs.lil.utils.vector.key;

import java.util.Objects;
import java.util.stream.Stream;

public class LexicalKey implements ISparseKey {
	private static final long serialVersionUID = -7189611280589009902L;
	private final String	s;
	private final int		hashCode;

	private LexicalKey(String s) {
		this.s = s;
		this.hashCode = Objects.hash(s);
	}

	public static LexicalKey of(String s) {
		return new LexicalKey(s);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other instanceof LexicalKey) {
			return Objects.equals(this.s, ((LexicalKey) other).s);
		} else {
			return false;
		}
	}

	@Override
	public Object getStructure() {
		return LexicalKey.class;
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public int lexicalSize() {
		return 1;
	}

	@Override
	public Stream<String> lexicalStream() {
		return Stream.of(s);
	}

	@Override
	public String toString() {
		return s;
	}
}
