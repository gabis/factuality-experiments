package edu.uw.cs.lil.utils.vector;

import edu.uw.cs.lil.utils.vector.key.ISparseKey;


public interface IMutableSparseVector extends ISparseVector {
	void addTimesToSelf(double coefficient, ISparseVector other);

	void addToSelf(ISparseVector other);

	void clear();

	void set(ISparseKey key, double value);

	void timesSelf(double coefficient);
}
