package edu.uw.cs.lil.exp.creator;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.exp.repository.IResourceRepository;

public class ResourceCreatorRepository implements
		IResourceCreatorRepository {
	private Map<String, IResourceCreator<?>> repository;
	
	public ResourceCreatorRepository() {
		this.repository = new HashMap<>();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T create(HierarchicalConfiguration config,
			IResourceRepository resourceRepo) {
		final String type = config.getString("type");
		if (!repository.containsKey(type)) {
			throw new IllegalArgumentException("Unable to find creator for "
					+ type);
		}
		return (T) repository.get(type).create(config, resourceRepo, this);
	}

	@Override
	public void registerResourceCreator(IResourceCreator<?> creator) {
		repository.put(creator.type(), creator);
	}
}
