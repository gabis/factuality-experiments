package edu.uw.cs.lil.exp.job;

import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.utils.ConfigUtils;

public class PrintJob extends AbstractJob {
	private static final Logger	log	= Logger.getLogger(PrintJob.class);

	private final String		message;

	public PrintJob(String id, Set<String> dependencies, String message) {
		super(id, dependencies);
		this.message = message;
	}

	@Override
	public void run() {
		log.info(message);
	}

	public static class Creator implements IResourceCreator<PrintJob> {
		@Override
		public PrintJob create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new PrintJob(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("input")));
		}

		@Override
		public String type() {
			return "job.print";
		}
	}
}
