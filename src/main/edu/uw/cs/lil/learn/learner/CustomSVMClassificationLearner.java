package edu.uw.cs.lil.learn.learner;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloQuadNumExpr;
import ilog.cplex.IloCplex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.data.tuple.Pair;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;

public class CustomSVMClassificationLearner<V> implements
		ILinearLearner<V, Boolean> {
	public static final Logger	log	= Logger.getLogger(CustomSVMClassificationLearner.class);
	private final double		c;

	public CustomSVMClassificationLearner(double c) {
		this.c = c;
	}

	@Override
	public void train(IModel<V, Boolean, IMutableSparseVector> model,
			IDataCollection<V> samples, Function<V, Boolean> labeler,
			ToDoubleFunction<V> varianceLabeler) {
		log.debug(String.format("Beginning training over %d samples",
				samples.size()));

		final ArrayList<Pair<ISparseVector, Double>> data = samples
				.stream()
				.parallel()
				.map(sample -> Pair.of(model.getFeatures(sample),
						labeler.apply(sample) ? 1.0 : -1.0))
				.collect(Collectors.toCollection(ArrayList::new));

		final ISparseKey[] featureMap = data.stream().map(Pair::first)
				.flatMap(ISparseVector::keyStream).distinct()
				.toArray(ISparseKey[]::new);
		final Map<ISparseKey, Integer> inverseFeatureMap = new HashMap<>();
		for (int i = 0; i < featureMap.length; i++) {
			inverseFeatureMap.put(featureMap[i], i);
		}

		log.info("Created feature map of size: " + featureMap.length);

		try {
			final IloCplex cplex = new IloCplex();
			final IloNumVar[] weights = cplex.numVarArray(featureMap.length,
					-Double.MAX_VALUE, Double.MAX_VALUE);
			final IloNumVar[] slacks = cplex.numVarArray(data.size(), 0,
					Double.MAX_VALUE);
			final IloQuadNumExpr weightIP = cplex.quadNumExpr();
			for (int i = 0; i < weights.length; i++) {
				weightIP.addTerm(0.5, weights[i], weights[i]);
			}
			final IloLinearNumExpr slackSum = cplex.linearNumExpr();
			for (int i = 0; i < slacks.length; i++) {
				slackSum.addTerm(c, slacks[i]);
			}
			final IloNumExpr objective = cplex.sum(weightIP, slackSum);
			cplex.addMinimize(objective);
			for (int i = 0; i < data.size(); i++) {
				final IloLinearNumExpr prediction = cplex.linearNumExpr();
				data.get(i)
						.first()
						.forEach(
								(key, value) -> {
									try {
										prediction.addTerm(value,
												weights[inverseFeatureMap
														.get(key)]);
									} catch (final IloException e) {
										throw new RuntimeException(e);
									}
								});
				final IloNumExpr lhs = cplex.prod(data.get(i).second(),
						prediction);
				final IloNumExpr rhs = cplex.diff(1, slacks[i]);
				cplex.addGe(lhs, rhs);
			}
			final boolean success = cplex.solve();
			if (!success) {
				throw new RuntimeException("cplex failed: " + cplex.getStatus());
			}

			final double[] solution = cplex.getValues(weights);
			model.getParams().clear();
			for (int i = 0; i < featureMap.length; i++) {
				model.getParams().set(featureMap[i], solution[i]);
			}
			cplex.end();
		} catch (final IloException e) {
			throw new RuntimeException(e);
		}
	}

	public static class Creator<V> implements
			IResourceCreator<CustomSVMClassificationLearner<V>> {
		@Override
		public CustomSVMClassificationLearner<V> create(
				HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new CustomSVMClassificationLearner<>(
					config.getDouble("cost"));
		}

		@Override
		public String type() {
			return "learner.svm.classification.custom";
		}
	}
}
