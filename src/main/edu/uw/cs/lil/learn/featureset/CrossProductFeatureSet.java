package edu.uw.cs.lil.learn.featureset;

import edu.uw.cs.lil.utils.vector.ISparseVector;

public class CrossProductFeatureSet<DI> implements IFeatureSet<DI> {
	private final IFeatureSet<DI>	f1;
	private final IFeatureSet<DI>	f2;

	public CrossProductFeatureSet(IFeatureSet<DI> f1, IFeatureSet<DI> f2) {
		this.f1 = f1;
		this.f2 = f2;
	}

	@Override
	public ISparseVector getFeatures(DI dataItem) {
		return f1.getFeatures(dataItem).crossProduct(f2.getFeatures(dataItem));
	}
}
