package edu.uw.cs.lil.events.exp.job.corpus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.events.data.EventsSentence;
import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.job.AbstractJob;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.utils.ConfigUtils;

public class CorpusBratJob extends AbstractJob {
	private final File			bratDirectory;
	private final EventsDataset	dataset;

	public CorpusBratJob(String id, Set<String> dependencies,
			EventsDataset dataset, File bratDirectory) {
		super(id, dependencies);
		this.bratDirectory = bratDirectory;
		this.dataset = dataset;
	}

	@Override
	public void run() {
		bratDirectory.mkdirs();
		try {
			for (final EventsDocument doc : dataset) {
				Files.write(getBratPath(doc.getId() + ".txt"),
						doc.toOriginalString().getBytes());
				int eventCount = 0;
				final List<String> annotationLines = new ArrayList<>();
				for (final EventsSentence sentence : doc) {
					for (final EventsToken token : sentence) {
						if (token.isEvent()) {
							eventCount++;
							annotationLines.add(String.format(
									"T%d\tEvent %d %d\t%s", eventCount,
									token.getCharStart(), token.getCharEnd(),
									token.getText()));
							annotationLines.add(String.format("E%d\tEvent:T%d",
									eventCount, eventCount));
							annotationLines
									.add(String
											.format("A%d\tFactuality E%d %s",
													eventCount, eventCount,
													Double.toString(token
															.getFactualityRating()
															.getMean())));
						}
					}
				}
				Files.write(getBratPath(doc.getId() + ".ann"), annotationLines);
			}
			final double[] factualities = IntStream.rangeClosed(-3 * 5, 3 * 5)
					.mapToDouble(i -> i / 5.0).toArray();
			Files.write(
					getBratPath("annotation.conf"), String
							.format("[entities]\n%s\n[relations]\n%s\n[events]\n%s\n[attributes]\n%s\n",
									"", "", "Event",
									"Factuality\tArg:<EVENT>, Value:"
											+ Arrays.stream(factualities)
													.mapToObj(Double::toString)
													.collect(Collectors
															.joining("|")))
					.getBytes());
			Files.write(getBratPath("visual.conf"),
					String.format("[labels]\n%s\n[drawing]\n%s\n", "",
							"Event\tbgColor:lightgreen, fgColor:black")
					.getBytes());
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Path getBratPath(String filename) {
		return Paths.get(bratDirectory.toString(), filename);
	}

	public static class Creator implements IResourceCreator<CorpusBratJob> {
		@Override
		public CorpusBratJob create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new CorpusBratJob(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("dataset")),
					ConfigUtils.getFile("brat", config));
		}

		@Override
		public String type() {
			return "job.corpus.brat";
		}
	}
}
