package edu.uw.cs.lil.events.exp.job.learn;

import java.util.Set;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.exp.job.AbstractJob;
import edu.uw.cs.lil.learn.learner.ILearner;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;

public abstract class AbstractLearningJob<V, A, P> extends AbstractJob {
	private static final Logger log = Logger
			.getLogger(AbstractLearningJob.class);

	public AbstractLearningJob(String id, Set<String> dependencies) {
		super(id, dependencies);
	}

	@Override
	public void run() {
		final IDataCollection<V> data = getData();
		log.info(String.format("Training with %d samples", data.size()));
		final ILearner<V, A, P> learner = getLearner();
		final IModel<V, A, P> model = getModel(data);
		learner.train(model, data, this::getLabel);
	}

	abstract protected IDataCollection<V> getData();

	abstract protected A getLabel(V sample);

	abstract protected ILearner<V, A, P> getLearner();

	abstract protected IModel<V, A, P> getModel(IDataCollection<V> data);
}
