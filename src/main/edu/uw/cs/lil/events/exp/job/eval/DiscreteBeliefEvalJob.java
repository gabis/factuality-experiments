package edu.uw.cs.lil.events.exp.job.eval;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.analysis.IAnalysis;
import edu.uw.cs.lil.events.analysis.stats.ContinuousEvaluationStats;
import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.ConfigUtils;
import edu.uw.cs.lil.utils.data.collection.FlatMappedDataCollection;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;

public class DiscreteBeliefEvalJob<P>
		extends AbstractEvalJob<EventsToken, Integer, P> {

	public static final Logger								log	= Logger
			.getLogger(DiscreteBeliefEvalJob.class);

	private final List<IAnalysis<EventsToken, Integer, P>>	analyses;
	private final ContinuousEvaluationStats<EventsToken, P>	evaluationAnalysis;
	private final IModel<EventsToken, Integer, P>			model;
	private final EventsDataset								testDataset;

	public DiscreteBeliefEvalJob(String id, Set<String> dependencies,
			EventsDataset testDataset, IModel<EventsToken, Integer, P> model) {
		super(id, dependencies);
		this.testDataset = testDataset;
		this.model = model;
		this.evaluationAnalysis = new ContinuousEvaluationStats<>();
		this.analyses = Arrays.asList();
	}

	@Override
	protected List<IAnalysis<EventsToken, Integer, P>> getAnalyses() {
		return analyses;
	}

	@Override
	protected Integer getLabel(EventsToken sample) {
		return (int) Math.round(sample.getFactualityRating().getMean());
	}

	@Override
	protected IModel<EventsToken, Integer, P> getModel() {
		return model;
	}

	@Override
	protected IDataCollection<EventsToken> getTestData() {
		return new FlatMappedDataCollection<>(testDataset, doc -> doc.stream()
				.flatMap(s -> s.stream()).filter(token -> token.isEvent()));
	}

	@Override
	protected void postProcess() {
		log.info(evaluationAnalysis);
	}

	@Override
	protected void sampleProcess(EventsToken sample) {
		evaluationAnalysis.analyze(sample,
				model.getResult(sample).doubleValue(),
				sample.getFactualityRating().getMean(), null);
	}

	public static class Creator<P>
			implements IResourceCreator<DiscreteBeliefEvalJob<P>> {
		@Override
		public DiscreteBeliefEvalJob<P> create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new DiscreteBeliefEvalJob<>(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("test")),
					resourceRepo.getResource(config.getString("model")));
		}

		@Override
		public String type() {
			return "job.belief.eval.discrete";
		}
	}
}