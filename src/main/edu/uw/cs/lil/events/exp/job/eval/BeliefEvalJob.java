package edu.uw.cs.lil.events.exp.job.eval;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.analysis.IAnalysis;
import edu.uw.cs.lil.events.analysis.stats.ContinuousEvaluationStats;
import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.ConfigUtils;
import edu.uw.cs.lil.utils.data.collection.FlatMappedDataCollection;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class BeliefEvalJob
		extends AbstractEvalJob<EventsToken, Double, IMutableSparseVector> {
	public static final Logger													log	= Logger
			.getLogger(BeliefEvalJob.class);

	private final List<IAnalysis<EventsToken, Double, IMutableSparseVector>>	analyses;
	private final ContinuousEvaluationStats<EventsToken, IMutableSparseVector>	evaluationAnalysis;
	private final IModel<EventsToken, Double, IMutableSparseVector>				model;
	private final EventsDataset													testDataset;

	public BeliefEvalJob(String id, Set<String> dependencies,
			EventsDataset testDataset,
			IModel<EventsToken, Double, IMutableSparseVector> model) {
		super(id, dependencies);
		this.testDataset = testDataset;
		this.model = model;
		this.evaluationAnalysis = new ContinuousEvaluationStats<>();
		this.analyses = Arrays.asList(evaluationAnalysis);
	}

	@Override
	protected List<IAnalysis<EventsToken, Double, IMutableSparseVector>> getAnalyses() {
		return analyses;
	}

	@Override
	protected Double getLabel(EventsToken sample) {
		return sample.getFactualityRating().getMean();
	}

	@Override
	protected IModel<EventsToken, Double, IMutableSparseVector> getModel() {
		return model;
	}

	@Override
	protected IDataCollection<EventsToken> getTestData() {
		return new FlatMappedDataCollection<>(testDataset, doc -> doc.stream()
				.flatMap(s -> s.stream()).filter(token -> token.isEvent()));
	}

	@Override
	protected void postProcess() {
		log.info(evaluationAnalysis);
	}

	@Override
	protected void sampleProcess(EventsToken sample) {
		log.info("************* SAMPLE START **************");
		log.info(sample);
		log.info("************* SAMPLE END **************");
	}

	public static class Creator implements IResourceCreator<BeliefEvalJob> {
		@Override
		public BeliefEvalJob create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new BeliefEvalJob(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("test")),
					resourceRepo.getResource(config.getString("model")));
		}

		@Override
		public String type() {
			return "job.belief.eval";
		}
	}
}
