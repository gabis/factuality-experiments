package edu.uw.cs.lil.events.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import edu.uw.cs.lil.events.services.EventsServices;
import edu.uw.cs.lil.events.timeml.EventInstance;
import edu.uw.cs.lil.events.timeml.TemporalRelationInstance;
import edu.uw.cs.lil.events.timeml.TimeMLAnnotation;
import edu.uw.cs.lil.events.timeml.TimexInstance;
import edu.uw.cs.lil.events.timeml.XMLElement;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.NumericalCrowdRating;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.data.text.ISituatedDocument;
import edu.uw.cs.lil.utils.data.text.IToken;
import edu.uw.cs.lil.utils.data.tuple.Pair;

public class EventsDocument implements
		ISituatedDocument<EventsToken, EventsSentence, EventsDocumentState> {
	public static final Logger								log					= Logger
			.getLogger(EventsDocument.class);

	private static final long								serialVersionUID	= 1329981977741397208L;

	public transient Map<Integer, Pair<Integer, Integer>>	beginCharToToken;
	public transient Map<String, XMLElement>				eidToElement;
	public transient Queue<XMLElement>						elements;
	public transient Map<Integer, Pair<Integer, Integer>>	endCharToToken;

	private final List<EventsSentence>						sentences;
	private final EventsDocumentState						state;

	public EventsDocument() {
		// Used only when reading the document for the first time
		this.sentences = new ArrayList<>();
		this.state = new EventsDocumentState();
		this.elements = new LinkedList<>();
		this.eidToElement = new HashMap<>();
		this.beginCharToToken = new HashMap<>();
		this.endCharToToken = new HashMap<>();
	}

	public EventsDocument(EventsDocumentState state) {
		this(new ArrayList<>(), state);
	}

	public EventsDocument(List<EventsSentence> sentences,
			EventsDocumentState state) {
		this.sentences = sentences;
		this.state = state;
	}

	public static EventsDocument deserialize(File inFile)
			throws IOException, ClassNotFoundException {
		try (final ObjectInputStream in = new ObjectInputStream(
				new FileInputStream(inFile))) {
			return (EventsDocument) in.readObject();
		}
	}

	public void addSentence(EventsSentence sentence) {
		this.sentences.add(sentence);
	}

	public void addXMLElement(XMLElement element) {
		if (element.getTag().equals("DOCID")) {
			getState().setId(element.getText());
		} else if (element.getTag().equals("TIMEX3") && "CREATION_TIME"
				.equals(element.getAttribute("functionInDocument"))) {
			getState().setDCT(new TimexInstance(0, -1, -1, -1,
					element.getAttribute("type"),
					element.getAttribute("value")));
			getState().getAnnotation().addTimexInstance(getState().getDCT());
		} else if (element.getTag().equals("TIMEX2")
				&& getState().getDCT() == null) {
			getState().setDCT(new TimexInstance(0, -1, -1, -1, "NONE",
					element.getAttribute("val")));
			getState().getAnnotation().addTimexInstance(getState().getDCT());
		} else if (element.getTag().equals("TEXT")) {
			for (final List<IToken> tokens : EventsServices
					.preprocessText(element.getText())) {
				sentences.add(new EventsSentence(tokens, sentences.size(), this,
						null));
			}
			this.stream().flatMap(s -> s.stream()).forEach(token -> {
				beginCharToToken.put(token.getCharStart(),
						Pair.of(token.getState().getIndex(), token.getIndex()));
				endCharToToken.put(token.getCharEnd(),
						Pair.of(token.getState().getIndex(), token.getIndex()));
			});
			if (!toOriginalString().equals(element.getText())) {
				log.info("Original: " + element.getText());
				log.info("Parsed: " + toOriginalString());
				throw new RuntimeException(
						"Parsed text different from original text: ");
			}
		} else {
			elements.add(element);
		}
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof EventsDocument) {
			return Objects.equals(this.getId(),
					((EventsDocument) other).getId());
		} else {
			return false;
		}
	}

	@Override
	public EventsSentence get(int i) {
		return i >= 0 && i < sentences.size() ? sentences.get(i) : null;
	}

	public TimeMLAnnotation getAnnotation() {
		return getState().getAnnotation();
	}

	@Override
	public String getId() {
		return getState().getId();
	}

	@Override
	public EventsDocumentState getState() {
		return state;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.getId());
	}

	@Override
	public Iterator<EventsSentence> iterator() {
		return sentences.iterator();
	}

	public void serialize(File outFile) {
		try (final ObjectOutputStream out = new ObjectOutputStream(
				new FileOutputStream(outFile))) {
			out.writeObject(this);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void setId(String id) {
		getState().setId(id);
	}

	@Override
	public int size() {
		return sentences.size();
	}

	@Override
	public Stream<EventsSentence> stream() {
		return sentences.stream();
	}

	public String toOriginalString() {
		if (sentences.isEmpty()) {
			return "";
		} else {
			return sentences.get(0).getPrefix() + stream()
					.map(s -> s.getExclusiveInclusiveSpan(0, s.size()))
					.collect(Collectors.joining());
		}
	}

	@Override
	public String toString() {
		return toTimeML(getAnnotation());
	}

	public String toTimeML(TimeMLAnnotation currentAnnotation) {
		final StringWriter w = new StringWriter();
		toTimeML(currentAnnotation, new StreamResult(w));
		return w.toString();
	}

	public void toTimeML(TimeMLAnnotation currentAnnotation, File f) {
		toTimeML(currentAnnotation, new StreamResult(f));
	}

	public void toTimeML(TimeMLAnnotation currentAnnotation,
			StreamResult result) {
		log.info("Creating TimeML.");
		try {
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			final org.w3c.dom.Document doc = docBuilder.newDocument();
			doc.setXmlStandalone(true);

			final Element rootElement = doc.createElement("TimeML");
			doc.appendChild(rootElement);

			final Element docidElement = doc.createElement("DOCID");
			docidElement.appendChild(doc.createTextNode(getId()));
			rootElement.appendChild(docidElement);

			final Element dctElement = doc.createElement("DCT");
			final Element dctTimexElement = doc.createElement("TIMEX3");
			dctTimexElement
					.appendChild(doc.createTextNode(getDCT().getValue()));
			dctTimexElement.setAttribute("tid", getDCT().formatId());
			dctTimexElement.setAttribute("type", getDCT().getType());
			dctTimexElement.setAttribute("value", getDCT().getValue());
			dctTimexElement.setAttribute("functionInDocument", "CREATION_TIME");
			dctElement.appendChild(dctTimexElement);
			rootElement.appendChild(dctElement);

			final Element textElement = doc.createElement("TEXT");

			final Map<Integer, PriorityQueue<EventInstance>> eventMap = new HashMap<>();
			for (final EventInstance e : currentAnnotation
					.getEventInstances()) {
				final PriorityQueue<EventInstance> eventQueue = eventMap
						.getOrDefault(e.getSentenceIndex(),
								new PriorityQueue<EventInstance>(
										(e1, e2) -> Integer.compare(
												e1.getStart(), e2.getStart())));
				eventQueue.add(e);
				eventMap.put(e.getSentenceIndex(), eventQueue);
			}

			int sentenceIndex = 0;
			if (this.size() > 0 && this.sentences.get(0).size() > 0) {
				textElement.appendChild(doc
						.createTextNode(sentences.get(0).get(0).getBefore()));
			}
			for (final EventsSentence sentence : this) {
				final PriorityQueue<EventInstance> sortedEvents = eventMap
						.getOrDefault(sentenceIndex, new PriorityQueue<>());
				int offset = 0;
				for (EventInstance e = sortedEvents.isEmpty() ? null
						: sortedEvents.poll(); e != null; e = sortedEvents
								.poll()) {
					if (offset != e.getStart()) {
						textElement.appendChild(doc.createTextNode(
								sentence.getExclusiveInclusiveSpan(offset,
										e.getStart())));
					}
					final Element eventElement = doc.createElement("EVENT");
					eventElement.appendChild(doc.createTextNode(sentence
							.getExclusiveSpan(e.getStart(), e.getEnd() + 1)));
					eventElement.setAttribute("class", e.getType());
					eventElement.setAttribute("eid", "e" + e.getId());
					textElement.appendChild(eventElement);
					textElement.appendChild(doc.createTextNode(
							sentence.get(e.getEnd()).getAfter()));
					offset = e.getEnd() + 1;
				}
				textElement.appendChild(doc.createTextNode(sentence
						.getExclusiveInclusiveSpan(offset, sentence.size())));
				sentenceIndex++;
			}
			rootElement.appendChild(textElement);

			for (final EventInstance e : currentAnnotation
					.getEventInstances()) {
				final Element eventInstanceElement = doc
						.createElement("MAKEINSTANCE");
				eventInstanceElement.setAttribute("eiid", e.formatId());
				eventInstanceElement.setAttribute("eventID", "e" + e.getId());
				rootElement.appendChild(eventInstanceElement);
			}

			for (final TemporalRelationInstance r : currentAnnotation
					.getRelationInstances()) {
				final Element relationElement = doc.createElement("LINK");
				relationElement.setAttribute("lid", r.formatId());
				relationElement.setAttribute("relType", r.getType());
				rootElement.appendChild(relationElement);
			}

			final Transformer transformer = TransformerFactory.newInstance()
					.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(new DOMSource(doc), result);
		} catch (final ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (final TransformerException e) {
			throw new RuntimeException(e);
		}
	}

	public EventsDocument withAnnotation(TimeMLAnnotation annotation) {
		final EventsDocument newDoc = new EventsDocument(
				new EventsDocumentState(this.state.getId(), this.state.getDCT(),
						annotation));
		this.sentences.stream().map(s -> new EventsSentence(s, newDoc))
				.forEach(newDoc.sentences::add);
		return newDoc;
	}

	public EventsDocument withDetection(
			IModel<EventsToken, Boolean, ?> detectionModel) {
		final TimeMLAnnotation a = new TimeMLAnnotation();
		this.sentences.stream().flatMap(IDataCollection::stream)
				.filter(detectionModel::getResult)
				.forEach(event -> a.addEventInstance(new EventInstance(
						a.getMaxEventId() + 1, event.getIndex(),
						event.getIndex(), event.getState().getIndex(),
						"OCCURRENCE", new NumericalCrowdRating(-1, 1),
						new NumericalCrowdRating(-1, 3), null)));
		return this.withAnnotation(a);
	}

	private TimexInstance getDCT() {
		return getState().getDCT();
	}
}
