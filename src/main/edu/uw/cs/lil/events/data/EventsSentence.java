package edu.uw.cs.lil.events.data;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.uw.cs.lil.events.timeml.TemporalInstance;
import edu.uw.cs.lil.utils.data.text.ISituatedSentence;
import edu.uw.cs.lil.utils.data.text.IToken;

public class EventsSentence implements
		ISituatedSentence<EventsToken, EventsDocument> {
	private static final long serialVersionUID = -4015900960202647029L;
	private final int				index;
	private final EventsDocument	state;
	private final List<EventsToken>	tokens;
	private final String			speaker;

	public EventsSentence(EventsSentence other, EventsDocument state) {
		this(other, other.index, state);
	}

	public EventsSentence(EventsSentence other, int index, EventsDocument state) {
		this.index = index;
		this.state = state;
		this.tokens = other.tokens.stream().map(t -> new EventsToken(t, this))
				.collect(Collectors.toList());
		this.speaker = other.speaker;
	}

	public EventsSentence(List<IToken> tokens, int index, EventsDocument state,
			String speaker) {
		this.index = index;
		this.state = state;
		this.tokens = tokens.stream().map(t -> new EventsToken(t, this))
				.collect(Collectors.toList());
		this.speaker = speaker;
	}

	@Override
	public EventsToken get(int i) {
		return i >= 0 && i < tokens.size() ? tokens.get(i) : null;
	}

	@Override
	public int getIndex() {
		return index;
	}

	public String getSpeaker() {
		return speaker;
	}

	@Override
	public EventsDocument getState() {
		return state;
	}

	public TemporalInstance getTemporalInstance(int tokenIndex) {
		return getState().getAnnotation().getTemporalInstance(this.getIndex(),
				tokenIndex);
	}

	@Override
	public Iterator<EventsToken> iterator() {
		return tokens.iterator();
	}

	@Override
	public int size() {
		return tokens.size();
	}

	@Override
	public Stream<EventsToken> stream() {
		return tokens.stream();
	}

	public String toDependencyString() {
		return stream().map(
				t -> String.format("%d:%s[%s](%s->%d)", t.getIndex(),
						t.getText(), t.getTag(), t.getDependencyType(),
						t.getGovernor())).collect(Collectors.joining(","));
	}

	@Override
	public String toString() {
		return stream().map(t -> t.getText()).collect(Collectors.joining(" "));
	}
}
