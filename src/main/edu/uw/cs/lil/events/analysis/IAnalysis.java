package edu.uw.cs.lil.events.analysis;

import edu.uw.cs.lil.learn.model.IModel;

public interface IAnalysis<V, A, P> {
	void analyze(V sample, A predicted, A gold, IModel<V, A, P> model);
}
