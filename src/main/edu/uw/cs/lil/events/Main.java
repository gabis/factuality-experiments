package edu.uw.cs.lil.events;

import java.io.File;
import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;

import edu.uw.cs.lil.events.exp.EventsExperiment;

/**
 * Main entry point for the events package.
 *
 * @author Kenton Lee
 */
public class Main {

	private Main() {
	}

	public static void main(String[] args) throws ConfigurationException,
			IOException {
		System.out.println("RUNNNING GABI OVERRIDES");
		if (args.length == 0) {
			usage();
		} else {
			new EventsExperiment(new File(args[0])).run();
		}
	}

	private static void usage() {
		System.out.println("Usage:");
		System.out.println("... <exp_file>");
		System.out.println("\tRun experiment.");
	}

}
