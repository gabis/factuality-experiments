package edu.uw.cs.lil.events.learn.featureset;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class RootParsePathFeatureSet implements IFeatureSet<EventsToken> {
	private static Stream<EventsToken> accumulateParents(EventsToken token) {
		if (token.getGovernor() < 0) {
			return Stream.of(token);
		} else {
			return Stream.concat(Stream.of(token), accumulateParents(token
					.getState().get(token.getGovernor())));
		}
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		return SparseVector.of(ObjectKey.of(RootParsePathFeatureSet.class,
				accumulateParents(dataItem).map(t -> t.getDependencyType())
						.collect(Collectors.joining("<="))));
	}
}
