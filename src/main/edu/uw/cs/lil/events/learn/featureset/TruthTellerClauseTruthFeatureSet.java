/**
 * @author gabis
 * Encode a TruthTeller Clause Truth value (CT)
 */
package edu.uw.cs.lil.events.learn.featureset;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

/*
 * Return CT value
 */
public class TruthTellerClauseTruthFeatureSet extends TruthTellerFeatureSet {

	public TruthTellerClauseTruthFeatureSet(HashMap<String, TruthTellerSentence> cache) {		
		super(cache);
		//System.out.println("Running CT");
	}
	
	// Returns the specific feature given a TT word instance
	@Override
	String getTruthtellerFeature(TruthTellerSingleWord ttWord){
		return ttWord.ct;
	}
		
	// Returns the default value of this specific feature 
	@Override
	String getDefaultValue(){
		return "P";
	}
}
