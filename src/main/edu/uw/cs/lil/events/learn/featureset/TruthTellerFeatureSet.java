package edu.uw.cs.lil.events.learn.featureset;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public abstract class TruthTellerFeatureSet  implements IFeatureSet<EventsToken> {
	HashMap<String, TruthTellerSentence> cache; // store tt's output per sentence to avoid calling 
												// multiple times per sentence
	
	public TruthTellerFeatureSet(HashMap<String, TruthTellerSentence> cache) {
		/*
		 * Get a cache instance, will be changed by reference.
		 */
		this.cache = cache;
	}
	
	public TruthTellerSentence runTruthTeller(String sentenceText) throws IOException{
		/*
		 * Get TruthTeller's response on a given sentence
		 * Maintains the cache
		 */
		// Try to get sentence from cache
		TruthTellerSentence ttSent = this.cache.get(sentenceText); 
		
		if (ttSent == null){
			// Not in cache -- call TT and store
			ttSent = new TruthTellerSentence(sentenceText); // create new instance
			String[] command = {
					"/home/gabis/factuality-project/src/run_TT_on_sent.sh",
					sentenceText
			};
		    Process process = Runtime.getRuntime().exec(command);                    
		    BufferedReader reader = new  BufferedReader(new InputStreamReader(        
		        process.getInputStream()));                                          
		    String s;
		    
		    // Add all words to TruthTeller's record
		    while ((s = reader.readLine()) != null) {
		    	ttSent.addTTOutputLine(s);		      
		    }
		    
		    // add the response to the cache
		    this.cache.put(sentenceText, ttSent);
		}
		else{
			//System.out.println("Loaded sentence from cache");
		}
		return ttSent;		
	}
	
	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		/*
		 * Return a sparse vector of this data item
		 */
		int wordIndex = dataItem.getIndex();
		String wordText = dataItem.getText();
		String sentenceText = dataItem.getState().toString();
		String retVal = this.getDefaultValue(); // Initialize with default return value
		
		TruthTellerSentence ttSent = null;
		try {	
			ttSent = this.runTruthTeller(sentenceText);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("TT crashed");
			e.printStackTrace();
		}
		
		// Once we have the relevant response from TT - check if the word
		// aligns (tokenization might be different)
		TruthTellerSingleWord curWordAnnot = ttSent.getWord(wordIndex);
		if (curWordAnnot != null){
			if (curWordAnnot.getWord().equalsIgnoreCase(wordText)){
				// If we found a match, change the return value from default
				// using this instance's specific feature extractor
				//System.out.println("Found match in TT!!! " + curWordAnnot.ct);
				retVal = this.getTruthtellerFeature(curWordAnnot);
			}
			else{
//				System.out.println(
//						"Parsed sent, but words didn't align:"
//								+ curWordAnnot.getWord() + " , "  
//								+ wordText);
			}
		}
			
		return SparseVector.of(
				ObjectKey.of(TruthTellerClauseTruthFeatureSet.class, retVal));
			
	}
	
	// Returns the specific feature given a TT word instance
	abstract String getTruthtellerFeature(TruthTellerSingleWord ttWord);
	
	// Returns the default value of this specific feature 
	abstract String getDefaultValue();
}
