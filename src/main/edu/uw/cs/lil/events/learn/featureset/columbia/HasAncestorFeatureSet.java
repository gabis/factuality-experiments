package edu.uw.cs.lil.events.learn.featureset.columbia;

import java.util.function.Predicate;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.CompositeKey;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class HasAncestorFeatureSet implements IFeatureSet<EventsToken> {
	private final String					label;
	private final Predicate<EventsToken>	ancestorPredicate;

	public HasAncestorFeatureSet(String label,
			Predicate<EventsToken> ancestorPredicate) {
		this.label = label;
		this.ancestorPredicate = ancestorPredicate;
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		if (dataItem.childStream().anyMatch(ancestorPredicate)) {
			return SparseVector.of(CompositeKey.of(
					ObjectKey.of(HasAncestorFeatureSet.class),
					ObjectKey.of(label)));
		} else {
			return SparseVector.EMPTY;
		}
	}
}
