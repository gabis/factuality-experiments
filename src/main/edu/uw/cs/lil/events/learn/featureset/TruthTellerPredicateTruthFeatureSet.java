package edu.uw.cs.lil.events.learn.featureset;

import java.util.HashMap;

public class TruthTellerPredicateTruthFeatureSet extends TruthTellerFeatureSet {
	public TruthTellerPredicateTruthFeatureSet(HashMap<String, TruthTellerSentence> cache) {
		super(cache);
	}

	// Returns the specific feature given a TT word instance
	@Override
	String getTruthtellerFeature(TruthTellerSingleWord ttWord) {
		//System.out.println("Running PT");
		return ttWord.pt;
	}

	// Returns the default value of this specific feature 
	@Override
	String getDefaultValue() {
		return "_";
	}
}
