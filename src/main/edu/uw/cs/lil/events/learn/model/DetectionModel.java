package edu.uw.cs.lil.events.learn.model;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.learn.featureset.BrownClusterFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.DependencyPathFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.LemmaFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.POSFeatureSet;
import edu.uw.cs.lil.events.learn.featureset.WordNetEventFeatureSet;
import edu.uw.cs.lil.events.lemma.WordNetLemmatizer;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.featureset.CrossProductFeatureSet;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.learn.model.LinearBinaryModel;

public class DetectionModel extends LinearBinaryModel<EventsToken> {
	public static final Logger log = Logger.getLogger(DetectionModel.class);

	public DetectionModel(boolean cached) {
		super(cached);
		addFeatureSet(new DependencyPathFeatureSet(1,
				t -> t.getText().toLowerCase(), 2, true));

		addFeatureSet(new LemmaFeatureSet(new WordNetLemmatizer()));

		final IFeatureSet<EventsToken> posFeatureSet = new POSFeatureSet();

		addFeatureSet(posFeatureSet);
		addFeatureSet(new CrossProductFeatureSet<>(new WordNetEventFeatureSet(),
				posFeatureSet));
		addFeatureSet(new CrossProductFeatureSet<>(
				new BrownClusterFeatureSet("c100.txt"), posFeatureSet));
		addFeatureSet(new CrossProductFeatureSet<>(
				new BrownClusterFeatureSet("c320.txt"), posFeatureSet));
		addFeatureSet(new CrossProductFeatureSet<>(
				new BrownClusterFeatureSet("c1000.txt"), posFeatureSet));
		addFeatureSet(new CrossProductFeatureSet<>(
				new BrownClusterFeatureSet("c3200.txt"), posFeatureSet));
	}

	public static class Creator implements IResourceCreator<DetectionModel> {
		private final String type;

		public Creator() {
			this("model.detection");
		}

		public Creator(String type) {
			this.type = type;
		}

		@Override
		public DetectionModel create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new DetectionModel(config.getBoolean("cached"));
		}

		@Override
		public String type() {
			return type;
		}
	}
}
