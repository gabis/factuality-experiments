package edu.uw.cs.lil.events.learn.featureset;

public class TruthTellerSingleWord {
	String sig, nu, ct, pt;  // The values annotated by TruthTeller per word
	String wordText; // The word's surface form
	int wordIndex; // The index of the word in the sentence
	
	public TruthTellerSingleWord(
			int wordIndex,
			String wordText,
			String sig,
			String nu,
			String ct,
			String pt
			) {
		this.sig = sig;
		this.nu = nu;
		this.ct = ct;
		this.pt = pt;
		this.wordText = wordText;
		this.wordIndex = wordIndex;
	}
	
	// Getters:
	public String getWord(){
		return this.wordText;
	}
	
	public String getSig(){
		return this.sig; 
	}
	
	public String getNU(){
		return this.nu; 
	}
	
	public String getCT(){
		return this.ct; 
	}
	
	public String getPT(){
		return this.pt; 
	}

}
