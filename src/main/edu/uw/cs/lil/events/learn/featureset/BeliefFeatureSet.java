package edu.uw.cs.lil.events.learn.featureset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.lemma.WordNetLemmatizer;
import edu.uw.cs.lil.learn.featureset.CompositeFeatureSet;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;

public class BeliefFeatureSet extends CompositeFeatureSet<EventsToken> {
	// store tt's output per sentence to avoid calling multiple times per sentence
	static HashMap<String, TruthTellerSentence> ttCache = new HashMap<String, TruthTellerSentence>(); 
	public BeliefFeatureSet() {
		super(getAllFeatureSets());
	}

	private static List<IFeatureSet<EventsToken>> getAllFeatureSets() {
		final List<IFeatureSet<EventsToken>> eventFeatureSets = getEventFeatureSets();
		return eventFeatureSets;
		/*
		 * return Stream.concat(eventFeatureSets.stream(),
		 * eventFeatureSets.stream().map(GoverningEventFeatureSet::new))
		 * .collect(Collectors.toList());
		 */
	}

	private static List<IFeatureSet<EventsToken>> getEventFeatureSets() {
		final List<IFeatureSet<EventsToken>> eventFeatures = new ArrayList<>();
		eventFeatures.add(new DependencyPathFeatureSet(2, t -> t.getText()
				.toLowerCase(), 2, true));
		eventFeatures.add(new LemmaFeatureSet(new WordNetLemmatizer()));
		eventFeatures.add(new POSFeatureSet());
        System.out.println("Adding Truthteller");
        eventFeatures.add(new TruthTellerSignatureFeatureSet(
        		BeliefFeatureSet.ttCache));
        eventFeatures.add(new TruthTellerNegationUncertaintyFeatureSet(
        		BeliefFeatureSet.ttCache));
		eventFeatures.add(new TruthTellerClauseTruthFeatureSet(
				BeliefFeatureSet.ttCache));
		eventFeatures.add(new TruthTellerPredicateTruthFeatureSet(
				BeliefFeatureSet.ttCache));
		return eventFeatures;
	}
}
