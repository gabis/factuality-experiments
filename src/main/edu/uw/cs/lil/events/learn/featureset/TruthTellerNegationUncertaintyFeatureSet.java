package edu.uw.cs.lil.events.learn.featureset;

import java.util.HashMap;

public class TruthTellerNegationUncertaintyFeatureSet extends TruthTellerFeatureSet {
	public TruthTellerNegationUncertaintyFeatureSet(HashMap<String, TruthTellerSentence> cache) {
		super(cache);
	}

	// Returns the specific feature given a TT word instance
	@Override
	String getTruthtellerFeature(TruthTellerSingleWord ttWord) {
		//System.out.println("Running NU");
		return ttWord.nu;
	}

	// Returns the default value of this specific feature 
	@Override
	String getDefaultValue() {
		return "_";
	}
}
