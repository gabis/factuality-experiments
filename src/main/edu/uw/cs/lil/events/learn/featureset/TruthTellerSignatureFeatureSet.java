package edu.uw.cs.lil.events.learn.featureset;

import java.util.HashMap;

public class TruthTellerSignatureFeatureSet extends TruthTellerFeatureSet {

	public TruthTellerSignatureFeatureSet(HashMap<String, TruthTellerSentence> cache) {
		super(cache);
	}

	// Returns the specific feature given a TT word instance
	@Override
	String getTruthtellerFeature(TruthTellerSingleWord ttWord) {
		//System.out.println("Running Signature");
		return ttWord.sig;
	}

	// Returns the default value of this specific feature 
	@Override
	String getDefaultValue() {
		return "_";
	}

}
