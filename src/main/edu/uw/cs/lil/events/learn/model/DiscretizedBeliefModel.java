package edu.uw.cs.lil.events.learn.model;

import java.util.Arrays;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.learn.featureset.BeliefFeatureSet;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.LinearOneVsRestModel;

public class DiscretizedBeliefModel extends
		LinearOneVsRestModel<EventsToken, Integer> {
	public DiscretizedBeliefModel(boolean cached) {
		super(Arrays.asList(new BeliefFeatureSet()), cached);
	}

	public static class Creator implements
			IResourceCreator<DiscretizedBeliefModel> {
		private final String	type;

		public Creator() {
			this("model.belief.discrete");
		}

		public Creator(String type) {
			this.type = type;
		}

		@Override
		public DiscretizedBeliefModel create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new DiscretizedBeliefModel(config.getBoolean("cached"));
		}

		@Override
		public String type() {
			return type;
		}
	}
}
