package edu.uw.cs.lil.events.learn.featureset;

import java.util.Optional;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class QuotationFeatureSet implements IFeatureSet<EventsToken> {
	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		final Optional<Integer> quoteStartIndex = dataItem
				.getState()
				.stream()
				.filter(t -> t.getText().equals("``")
						|| t.getText().equals("`"))
				.min((x, y) -> Integer.compare(x.getIndex(), y.getIndex()))
				.map(EventsToken::getIndex);
		final Optional<Integer> quoteEndIndex = dataItem
				.getState()
				.stream()
				.filter(t -> t.getText().equals("''")
						|| t.getText().equals("'"))
				.min((x, y) -> Integer.compare(x.getIndex(), y.getIndex()))
				.map(EventsToken::getIndex);
		final boolean hasQuote = quoteStartIndex.isPresent()
				&& quoteEndIndex.isPresent()
				&& quoteStartIndex.get() < dataItem.getIndex()
				&& quoteEndIndex.get() > dataItem.getIndex();
		return SparseVector.of(
				ObjectKey.of(QuotationFeatureSet.class, hasQuote), 1);
	}
}
