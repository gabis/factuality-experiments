package edu.uw.cs.lil.events.learn.featureset.columbia;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.CompositeKey;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class ChildAuxTypeFeatureSet implements IFeatureSet<EventsToken> {
	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		return SparseVector.of(CompositeKey.of(ObjectKey
				.of(ChildAuxTypeFeatureSet.class), ObjectKey.of(dataItem
				.childStream()
				.filter(child -> VerbTypeFeatureSet.getVerbType(child).equals(
						"aux")).map(EventsToken::getText)
				.map(String::toLowerCase).findFirst().orElse("nil"))));
	}
}
