package edu.uw.cs.lil.events.learn.featureset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TruthTellerSentence {
	String sentenceText;
	HashMap<Integer, TruthTellerSingleWord> words; // Map from word index to feats
	
	public TruthTellerSentence(
			String sentenceText   // the sentence text
			){
		this.sentenceText = sentenceText;
		this.words = new HashMap<Integer, TruthTellerSingleWord>();
	}
	
	public TruthTellerSingleWord getWord(int index){
		/*
		 * Return the word at a given index
		 */
		return this.words.get(new Integer(index));
	}
	
	public void addTTOutputLine(String ttOutput){
		/*
		 * Parse a line of TT's output pertaining to a single word
		 */
		ttOutput = ttOutput.replace("\n", "").replace("\r", "");
		String[] data = new String[5];
		data = ttOutput.split("\t");
		Integer wordIndex = Integer.parseInt(data[0]);
		this.words.put(
				wordIndex, 
				new TruthTellerSingleWord(
						wordIndex, 
						data[1], data[2], data[3], data[4], data[5]));
		
	}

}
