package edu.uw.cs.lil.events.learn.featureset;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.CompositeKey;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class GoverningEventFeatureSet implements IFeatureSet<EventsToken> {
	private final IFeatureSet<EventsToken>	baseFeatureSet;

	public GoverningEventFeatureSet(IFeatureSet<EventsToken> baseFeatureSet) {
		this.baseFeatureSet = baseFeatureSet;
	}

	private static EventsToken findGoverningEvent(EventsToken dataItem) {
		final EventsToken governor = dataItem.getState().get(
				dataItem.getGovernor());
		// if (governor == null || governor.getTag().startsWith("VB")) {
		if (governor == null || governor.isEvent()) {
			return governor;
		} else {
			return findGoverningEvent(governor);
		}
	}

	public IFeatureSet<EventsToken> getBaseFeatureSet() {
		return baseFeatureSet;
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		final EventsToken governor = findGoverningEvent(dataItem);
		if (governor == null) {
			return SparseVector.EMPTY;
		} else {
			return baseFeatureSet.getFeatures(governor).mapKeys(
					(k, v) -> CompositeKey.of(
							ObjectKey.of(GoverningEventFeatureSet.class), k));
		}
	}
}
