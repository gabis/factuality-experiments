package edu.uw.cs.lil.events.learn.featureset;

import java.util.Optional;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.external.distributional.IWordClusters;
import edu.uw.cs.lil.events.services.EventsServices;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class BrownClusterFeatureSet implements IFeatureSet<EventsToken> {

	private final String	clustersName;

	public BrownClusterFeatureSet(String clustersName) {
		this.clustersName = clustersName;
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		final IWordClusters clusters = EventsServices
				.getBrownClusters(clustersName);
		return Optional
				.ofNullable(clusters.getCluster(dataItem.getText()))
				.<ISparseVector> map(
						c -> SparseVector.of(ObjectKey.of(
								BrownClusterFeatureSet.class,
								clusters.getClusteringType(), c)))
				.orElse(SparseVector.EMPTY);
	}
}
