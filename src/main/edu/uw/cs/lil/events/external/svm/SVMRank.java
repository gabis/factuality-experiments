package edu.uw.cs.lil.events.external.svm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.utils.data.tuple.Pair;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;

public class SVMRank {
	private static final Logger	log	= Logger.getLogger(SVMRank.class);

	private SVMRank() {
	}

	public static ISparseVector learnModel(List<Sample> samples) {
		log.debug(String.format("Beginning training over %d samples",
				samples.size()));

		final ISparseKey[] featureMap = SVMLightUtils.getFeatureMap(samples);
		final Map<ISparseKey, Integer> inverseFeatureMap = SVMLightUtils
				.getInverseFeatureMap(featureMap);

		log.debug(String.format("Created feature map of size %d",
				featureMap.length));

		try {
			final File trainingFile = File.createTempFile("temp", ".dat");
			try (final BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(trainingFile)))) {
				for (final Sample sample : samples) {
					writer.write(sample.toString(inverseFeatureMap));
				}
			}
			log.debug("Training file: " + trainingFile.getAbsolutePath());

			final File modelFile = File.createTempFile("temp", ".model");

			final ProcessBuilder processBuilder = new ProcessBuilder(
					"resources/svm_rank/svm_rank_learn", "-c", "1.0",
					trainingFile.getAbsolutePath(), modelFile.getAbsolutePath())
					.redirectErrorStream(true);

			final Process process = processBuilder.start();
			final BufferedReader br = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			String line;
			log.debug("Running: "
					+ processBuilder.command().stream()
							.collect(Collectors.joining(" ")));
			while ((line = br.readLine()) != null) {
				log.debug(line);
			}
			final int exitValue = process.waitFor();
			log.debug("Exit Value is " + exitValue);

			trainingFile.delete();

			log.debug("Model file: " + modelFile.getAbsolutePath());
			final ISparseVector weights = SVMLightUtils.readModel(modelFile,
					featureMap);
			modelFile.delete();
			return weights;
		} catch (final IOException | InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public static double testModel(ISparseVector weights,
			List<SVMRank.Sample> samples, String label) {
		log.debug(weights);
		int totalComparisons = 0;
		int correctComparisons = 0;
		for (final SVMRank.Sample sample1 : samples) {
			for (final SVMRank.Sample sample2 : samples) {
				if (sample1 != sample2) {
					totalComparisons++;
					final double score1 = sample1.features.dotProduct(weights);
					final double score2 = sample2.features.dotProduct(weights);
					final boolean goldRanking = sample1.target > sample2.target;
					final boolean predictedRanking = score1 > score2;
					if (goldRanking == predictedRanking) {
						correctComparisons++;
					}
				}
			}
		}
		log.debug(String.format("%d/%d (%.3f%%) %s comparisons correct.",
				correctComparisons, totalComparisons, correctComparisons
						* 100.0 / totalComparisons, label));
		return correctComparisons / (double) totalComparisons;
	}

	public static class Sample extends AbstractSVMSample<Double> {
		public final int	constraintCluster;

		private Sample(double target, double cost, ISparseVector features,
				int constraintCluster) {
			super(target, cost, features);
			this.constraintCluster = constraintCluster;
		}

		public static Sample of(double rankingValue, double cost,
				ISparseVector features, int constraintCluster) {
			return new Sample(rankingValue, cost, features, constraintCluster);
		}

		@Override
		public String toString(Map<ISparseKey, Integer> inverseFeatureMap) {
			return String.format(
					"%f cost:%f qid:%d %s\n",
					target,
					cost,
					constraintCluster,
					features.stream()
							.map(entry -> Pair.of(
									inverseFeatureMap.get(entry.first()),
									entry.second()))
							.sorted((x, y) -> Integer.compare(x.first(),
									y.first()))
							.map(entry -> entry.first() + 1 + ":"
									+ entry.second())
							.collect(Collectors.joining(" ")));
		}
	}

}
