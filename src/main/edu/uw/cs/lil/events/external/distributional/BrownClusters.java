package edu.uw.cs.lil.events.external.distributional;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.stream.Collectors;

public class BrownClusters implements IWordClusters {
	private final String				clusteringType;
	private final Map<String, String>	clusters;

	public BrownClusters(File clusterFile) {
		clusteringType = clusterFile.getName();
		try {
			clusters = Files.lines(clusterFile.toPath()).collect(
					Collectors.toMap(s -> s.split("\t")[1],
							s -> s.split("\t")[0]));
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getCluster(String word) {
		return clusters.get(word);
	}

	@Override
	public String getClusteringType() {
		return clusteringType;
	}
}