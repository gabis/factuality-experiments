package edu.uw.cs.lil.events.external.svm;

import java.util.Map;

import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;

public abstract class AbstractSVMSample<T> {
	public final T				target;
	public final double			cost;
	public final ISparseVector	features;

	protected AbstractSVMSample(T target, double cost, ISparseVector features) {
		this.target = target;
		this.cost = cost;
		this.features = features;
	}

	abstract String toString(Map<ISparseKey, Integer> inverseFeatureMap);
}
