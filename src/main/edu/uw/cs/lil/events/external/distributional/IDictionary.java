package edu.uw.cs.lil.events.external.distributional;

import java.util.List;

public interface IDictionary<R> {
	String getMostSimilar(String word);

	double getSimilarity(String w1, String w2);

	List<String> getTopSimilarity(String word, int k);

	R lookup(String word);
}