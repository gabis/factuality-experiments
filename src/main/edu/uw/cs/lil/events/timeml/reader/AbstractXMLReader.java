package edu.uw.cs.lil.events.timeml.reader;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import jregex.Matcher;
import jregex.Pattern;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.events.timeml.XMLElement;
import edu.uw.cs.lil.utils.data.tuple.Pair;

public abstract class AbstractXMLReader extends DefaultHandler implements
		IEventsReader {
	public static final Logger						log				= Logger.getLogger(AbstractXMLReader.class);
	private static final Pattern					TIMEML_ID_REGEX	= new Pattern(
																			"({prefix}t|e|ei|l|E|T|TL|S|SECTIME|Sectime)({id}\\d+)");

	private final StringBuffer						fullDocumentText;
	private final SAXParser							sp;
	private final Map<String, Stack<XMLElement>>	tagMap;
	protected EventsDocument						document;

	public AbstractXMLReader() {
		try {
			sp = SAXParserFactory.newInstance().newSAXParser();
		} catch (final SAXException | ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
		tagMap = new HashMap<>();
		fullDocumentText = new StringBuffer();
	}

	protected static Integer getTimeMLId(String id) {
		final Matcher matcher = TIMEML_ID_REGEX.matcher(id);
		if (matcher.matches()) {
			return Integer.parseInt(matcher.group("id"));
		} else {
			return null;
		}
	}

	@Override
	public void characters(char[] buffer, int offset, int length) {
		tagMap.forEach((tag, elements) -> elements.forEach(e -> e.appendText(
				buffer, offset, length)));
		fullDocumentText.append(buffer, offset, length);
	}

	@Override
	public EventsDocument doReadDocument(File f) {
		document = new EventsDocument();
		preProcess(f);
		try {
			sp.parse(f, this);
		} catch (final SAXException e) {
			throw new RuntimeException(e);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		postProcess(document);
		tagMap.clear();
		fullDocumentText.setLength(0);
		return document;
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (!tagMap.containsKey(qName)) {
			return;
		}
		final Stack<XMLElement> elementStack = tagMap.get(qName);
		document.addXMLElement(elementStack.pop());
		if (elementStack.isEmpty()) {
			tagMap.remove(qName);
		}
	}

	public Pair<Integer, Integer> findToken(
			Map<Integer, Pair<Integer, Integer>> charToToken, int charOffset,
			boolean lookForwards) {
		Pair<Integer, Integer> candidate;
		candidate = charToToken.get(charOffset);
		if (candidate != null) {
			return candidate;
		}
		candidate = charToToken.get(charOffset + 1);
		if (candidate != null) {
			return candidate;
		}
		candidate = charToToken.get(charOffset - 1);
		if (candidate != null) {
			return candidate;
		}
		return lookForwards ? findToken(charToToken, charOffset + 2,
				lookForwards) : findToken(charToToken, charOffset - 2,
				lookForwards);
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if (!getRelevantTags().contains(qName)) {
			return;
		}
		final Stack<XMLElement> elementStack = tagMap.getOrDefault(qName,
				new Stack<XMLElement>());
		elementStack.add(new XMLElement(qName, attributes, getTextOffset()));
		tagMap.put(qName, elementStack);
	}

	private int getTextOffset() {
		if (!tagMap.containsKey("TEXT")) {
			return -1;
		} else {
			return tagMap.get("TEXT").peek().getText().length();
		}
	}

	protected abstract Set<String> getRelevantTags();

	protected abstract void postProcess(EventsDocument doc);

	protected abstract void preProcess(File f);
}