package edu.uw.cs.lil.events.timeml;

import edu.uw.cs.lil.events.data.EventsDocument;

public class TimexInstance extends TemporalInstance {
	private static final long	serialVersionUID	= -453265404909908288L;
	private final String		type;
	private final String		value;

	public TimexInstance(int id, int start, int end, int sentenceIndex,
			String type, String value) {
		super(id, start, end, sentenceIndex);
		this.type = type;
		this.value = value;
	}

	@Override
	public String formatId() {
		return "t" + getId();
	}

	public String getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toPrettyString(EventsDocument document) {
		if (getStart() < 0 || getEnd() < 0) {
			// DCT
			return String.format("%-6s:%-12s(DCT)", formatId(), value);
		} else {
			return String.format(
					"%-6s:%-12s(%s)",
					formatId(),
					value,
					document.get(getSentenceIndex()).getExclusiveSpan(
							getStart(), getEnd() + 1));
		}
	}

	@Override
	public String toString() {
		if (getStart() < 0 || getEnd() < 0) {
			// DCT
			return String.format("%-6s:%-12s(DCT)");
		} else {
			return String.format("%-6s:%-12s[%d-%d]", formatId(), value,
					getStart(), getEnd());
		}
	}
}
