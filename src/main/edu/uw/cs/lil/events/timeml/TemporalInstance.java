package edu.uw.cs.lil.events.timeml;

import edu.uw.cs.lil.events.data.EventsSentence;

public abstract class TemporalInstance extends TimeMLInstance {
	private static final long	serialVersionUID	= 7398662430538469825L;
	private final int			end;
	private final int			sentenceIndex;
	private final int			start;

	public TemporalInstance(int id, int start, int end, int sentenceIndex) {
		super(id);
		this.start = start;
		this.end = end;
		this.sentenceIndex = sentenceIndex;
	}

	public int getEnd() {
		return end;
	}

	public int getSentenceIndex() {
		return sentenceIndex;
	}

	public int getStart() {
		return start;
	}

	@Override
	public boolean isRelevant(EventsSentence sentence) {
		return sentenceIndex == sentence.getIndex();
	}
}
