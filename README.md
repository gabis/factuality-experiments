# Event Detection and Factuality Assessment with Non-Expert Supervision
This repository contains the code and data to replicate the results from the paper (http://www.kentonl.com/pub/lacz-emnlp.2015.pdf).

If you are just looking for the data, see https://bitbucket.org/kentonl/factuality-data.

## Dependencies
* Java 8
* Maven
* CPLEX

## Setting Up
* Run `download_external.sh` from the `resources` directory.
* CPLEX:

    * Make sure that the version-specific jar file, copied from CPLEX's installation (under `$CPLEX-INSTALLATION/lib/cplex.jar`), is placed under [lib/cplex/cplex/](lib/cplex/cplex/).
    For example, the jar file for CPLEX 12.6 is in [lib/cplex/cplex/12.6/cplex-12.6.jar](lib/cplex/cplex/12.6/cplex-12.6.jar)

    * Edit `pom.xml`:

        * Replace `-Djava.library.path=/opt/ibm/ILOG/CPLEX_Studio1261/cplex/bin/x86-64_linux` with the path to CPLEX.
        * Indicate the CPLEX version under the cplex dependency, e.g., `<version>12.6</version>`



## Running Experiments
The `experiments` directory contains XML files that specify the workflows to produce the results from the paper. To run an experiment, use `doExperiment.sh`. For example, to replicate the development results for event detection, run `./doExperiment.sh experiments/devDetectionEval.xml`
